package com.rognet.testapplication.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import com.rognet.testapplication.R
import com.rognet.testapplication.UrlModel
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.viewholder_data.*

class DataViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun bindItem(item: UrlModel) = with(item) {
        latitudeTextView.text = containerView.resources.getString(R.string.latitude, latitiude)
        longitudeTextView.text = containerView.resources.getString(R.string.longitude, longitude)
        batteryTextView.text = containerView.resources.getString(R.string.battery, battery)
    }
}