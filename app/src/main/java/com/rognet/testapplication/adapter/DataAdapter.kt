package com.rognet.testapplication.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.rognet.testapplication.R
import com.rognet.testapplication.UrlModel

class DataAdapter(val context: Context, private var items: ArrayList<UrlModel>) :
        RecyclerView.Adapter<DataViewHolder>() {

    private val inflater by lazy { LayoutInflater.from(context) }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            DataViewHolder(inflater.inflate(R.layout.viewholder_data, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.bindItem(items[position])
    }
}