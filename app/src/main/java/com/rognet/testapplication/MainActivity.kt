package com.rognet.testapplication

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import com.rognet.testapplication.adapter.DataAdapter
import com.rognet.testapplication.utils.LIST_SIZE
import com.rognet.testapplication.utils.TIME_BATTERY
import com.rognet.testapplication.utils.TIME_LOCATION
import com.rognet.testapplication.utils.URL
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : MvpActivity<MainContract.View, MainContract.Presenter>(),
        MainContract.View {

    private lateinit var adapter: DataAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.onViewCreated(this)
        initView()
    }

    override fun createPresenter() = MainPresenter()

    private fun initView() {
        aTextView.text = getString(R.string.a, TIME_LOCATION)
        bTextView.text = getString(R.string.b, TIME_BATTERY)
        cTextView.text = getString(R.string.c, LIST_SIZE)
        urlTextView.text = getString(R.string.url, URL)
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = DataAdapter(this, ArrayList<UrlModel>())
        recyclerView.adapter = adapter
        startButton.setOnClickListener {
            presenter.onStartThreads()
        }
        stopButton.setOnClickListener {
            presenter.onStopThreads()
        }
    }

    override fun updateView(data: ArrayList<UrlModel>) {
        recyclerView.adapter = DataAdapter(this, data)
    }

    override fun showSentSuccess() {
        Toast.makeText(this, "Sent success", Toast.LENGTH_SHORT).show()
    }

    override fun showSentError() {
        Toast.makeText(this, "Sent error", Toast.LENGTH_SHORT).show()
    }

    override fun startButtonActive(active: Boolean) {
        startButton.isEnabled = active
    }
}
