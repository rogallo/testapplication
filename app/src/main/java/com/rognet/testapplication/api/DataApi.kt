package com.rognet.testapplication.api

import com.rognet.testapplication.UrlModel
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.Body
import retrofit2.http.POST

interface DataApi {

    @POST("data")
    fun sentData(@Body urlModelList: ArrayList<UrlModel>): Observable<ResponseBody>
}