package com.rognet.testapplication.api

import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.rognet.testapplication.BuildConfig
import com.rognet.testapplication.utils.URL
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import okhttp3.logging.HttpLoggingInterceptor.Level.NONE
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitFactory<out ApiInterface>(private val apiClass: Class<ApiInterface>) {

    fun <T> createRetrofitQuery(func: (ApiInterface) -> Observable<T>) =
            Single
                    .just(createRetrofit())
                    .observeOn(Schedulers.io())
                    .flatMapObservable { func(it.create(apiClass)) }
                    .doOnNext { Log.d("Retrofit", Gson().toJson(it)) }
                    .observeOn(AndroidSchedulers.mainThread())

    private fun createRetrofit() =
            Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create(generateGsonReader()))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(getHttpClient())
                    .build()

    private fun generateGsonReader() =
            GsonBuilder()
                    .setLenient()
                    .create()

    private fun getHttpClient() =
            OkHttpClient.Builder()
                    .addLoggingInterceptor(if (BuildConfig.DEBUG) BODY else NONE)
                    .build()

    private fun OkHttpClient.Builder.addLoggingInterceptor(level: HttpLoggingInterceptor.Level) =
            addNetworkInterceptor(HttpLoggingInterceptor().setLevel(level))
}