package com.rognet.testapplication

import android.content.Context
import com.hannesdorfmann.mosby3.mvp.MvpPresenter
import com.hannesdorfmann.mosby3.mvp.MvpView

interface MainContract {

    interface View : MvpView {
        fun updateView(data: ArrayList<UrlModel>)
        fun showSentSuccess()
        fun showSentError()
        fun startButtonActive(active: Boolean)
    }

    interface Presenter : MvpPresenter<View> {
        fun onViewCreated(context: Context)
        fun onStartThreads()
        fun onStopThreads()
    }
}