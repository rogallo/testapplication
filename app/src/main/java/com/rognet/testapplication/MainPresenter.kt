package com.rognet.testapplication

import android.content.Context
import android.util.Log
import com.google.android.gms.location.LocationRequest
import com.hannesdorfmann.mosby3.mvp.MvpNullObjectBasePresenter
import com.rognet.testapplication.api.DataApi
import com.rognet.testapplication.api.RetrofitFactory
import com.rognet.testapplication.utils.LIST_SIZE
import com.rognet.testapplication.utils.TIME_BATTERY
import com.rognet.testapplication.utils.TIME_LOCATION
import com.rognet.testapplication.utils.getBatteryPercentage
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import net.kjulio.rxlocation.RxLocation

class MainPresenter : MvpNullObjectBasePresenter<MainContract.View>(),
        MainContract.Presenter {

    private var dataApi = RetrofitFactory(DataApi::class.java)
    private lateinit var context: Context
    private lateinit var t1: T1
    private lateinit var t2: T2
    private lateinit var t3: T3

    override fun onViewCreated(context: Context) {
        this.context = context
    }

    override fun onStartThreads() {
        t3 = T3("T3 Thread")
        t3.start()
        view.updateView(ArrayList<UrlModel>())
        view.startButtonActive(false)
    }

    override fun onStopThreads() {
        t1.stopRunning()
        t2.stopRunning()
        t3.stopRunning()
    }

    private inner class T1(name: String) : Thread(name) {

        private var isRunning = true

        override fun run() {
            var defaultLocationRequest = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(TIME_LOCATION)

            if (isRunning) {
                Log.d("T1", "Run " + Thread.currentThread().name)
                RxLocation.locationUpdates(context, defaultLocationRequest)
                        .subscribe { location ->
                            t3.latitude = location.latitude.toString()
                            t3.longitude = location.longitude.toString()
                        }
            }
        }

        fun stopRunning() {
            isRunning = false
        }
    }

    private inner class T2(name: String) : Thread(name) {

        private var isRunning = true

        override fun run() {
            while (isRunning) {
                Log.d("T2", "Run " + Thread.currentThread().name)
                t3.mainBattery = getBatteryPercentage(context).toString() + "%"
                try {
                    Thread.sleep(TIME_BATTERY)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        fun stopRunning() {
            isRunning = false
        }
    }

    private inner class T3(name: String) : Thread(name) {

        var latitude: String = ""
        var longitude: String = ""
        var mainBattery: String = ""
        var list = ArrayList<UrlModel>()
        var isRunning = true

        override fun run() {
            t1 = T1("T1 Thread")
            t1.start()
            t2 = T2("T2 Thread")
            t2.start()
            while (isRunning) {
                Log.d("T3", "Run " + Thread.currentThread().name)
                Log.d("T3", "Data $latitude $longitude $mainBattery")
                if (latitude.isNotEmpty() &&
                        longitude.isNotEmpty() && mainBattery.isNotEmpty()) {
                    addToList()
                    latitude = ""
                    longitude = ""
                    mainBattery = ""
                }
                try {
                    Thread.sleep(4000)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        fun stopRunning() {
            isRunning = false
            list.clear()
        }

        private fun addToList() {
            if (list.size <= LIST_SIZE) {
                list.add(UrlModel(latitude, longitude, mainBattery))
                updateView(list)
            }
            if (list.size == LIST_SIZE) {
                sent(list)
                isRunning = false
                Log.d("T3", "List " + list.size)
            }
        }

        private fun updateView(data: ArrayList<UrlModel>) {
            Observable
                    .just(data)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeBy(
                            onNext = { view.updateView(data) },
                            onError = {
                                view.showSentError()
                                list.clear()
                                Log.d("Update view", it.message)
                            })
        }

        private fun sent(list: ArrayList<UrlModel>) {
            Observable
                    .just(Unit)
                    .flatMap { it ->
                        dataApi.createRetrofitQuery {
                            it.sentData(list)
                        }
                    }
                    .subscribeBy(
                            onNext = {
                                view.showSentSuccess()
                                refresh()
                            },
                            onError = {
                                view.showSentError()
                                refresh()
                                Log.d("Retrofit", it.message)
                            })
        }

        private fun refresh(){
            view.startButtonActive(true)
            list.clear()
            onStopThreads()
        }
    }
}